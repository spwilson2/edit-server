import os
import json
import functools
import socket
import threading
import logging

LOG_NAME = 'pyvim.log'

class AutocomamndProps:
    AU_PROP = '_autocommand_properties'
    def __init__(self, name, context):
        self.name = name
        self.context = context

        self.func = None

    @classmethod
    def mark(cls, func, name, context):
        setattr(func, cls.AU_PROP, cls(name, context))

    @classmethod
    def get_from(cls, func):
        self = getattr(func, cls.AU_PROP, None)
        if self is not None:
            self.func = func
        return self


def log(*args):
    logging.debug(*args)

def _init_logging(filename=None):
    if filename is None:
        path = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
        filename = os.path.join(path, LOG_NAME)
    logging.basicConfig(filename=filename, filemode='w', level=logging.DEBUG)

def plugin(cls):
    '''
    This decorator marks the given `cls` as a plugin.

    The plugin will be initialized automatically as part of this framework.
    '''
    _PluginManager.get().plugins.append(cls)
    return cls

def autocommand(name, context='*'):
    '''
    This decoratated function provides an autocommand callback.
    '''
    def wrap(func):
        AutocomamndProps.mark(func, name, context)
        return func
    return wrap

def initialize_plugins(vim):
    _init_logging()

    manager = _PluginManager.get()
    def _init():
        manager.init(vim)
        # Initialize autocommand hooks within vim
        manager.init_plugins()
        manager.init_all_autocommands()

    manager.thread = threading.Thread(target=_init)
    manager.thread.daemon = True
    manager.thread.start()

def async_command(vim):
    _PluginManager.get().async_command(command)

def sync_command(command):
    _PluginManager.get().synchronous_command(command)

def sync_eval(vim):
    # TODO Using a scheduled user function:
    #
    pass

def serialize(data):
    return json.dumps(data).encode('utf-8')

def _synchronous_hook(vim):
    log('Called _synchronous_hook')
    _PluginManager.get()._handle_synchronous_command(vim)

def _autocommand_hook(vim):
    au = vim.eval("a:name")
    log('Called _autocommand_hook: %s' % au)
    _PluginManager.get().handle_autocommand(au)

class _VimChannel():
    # NOTE: eval will be much more difficult to implement asynchonously.
    #
    # Since we use TCP messages may be buffered and JSON will be difficult to
    # parse using the python json module.
    #
    # (We could use a JSONDecodeError to do assist with this, but I don't want
    # to think about buffering data yet..)
    def __init__(self, vim):
        self.vim = vim

        self._sock = None
        self._message_idx = -1

    def connect(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind(('localhost', 0))
            s.listen(1)

            port = s.getsockname()[1]
            self.vim.command('let channel = ch_open("localhost:%d")' % port)
            s.settimeout(.5)
            conn, _addr = s.accept() # NOTE Throws on timeout

            self._sock = conn
            self._message_idx = -1

    def send_command(self, data):
        log('Sending: %s' % repr(data))
        self._sock.send(serialize(['ex', data]))


class _PluginManager:
    _manager = None

    def __init__(self):
        # List of the registered plugin classes
        self.plugins = []
        # List of plugin classes instantiations
        self.plugin_objs = []
        self.plugin_autocommands = {}

        self.vim_sock = None
        self.vim = None

    @classmethod
    def get(cls):
        if cls._manager is None:
            cls._manager = cls()
        return cls._manager

    @classmethod
    def init(cls, vim):
        self = cls.get()
        self.vim = vim

        # Instantiate a socket connectin to vim
        self.vim_chan = _VimChannel(vim)
        self.vim_chan.connect()

        # Init the hook used to get synchronous commands
        func = '''
        function! PyVimSyncCB() 
            echom "called PyVimSyncCB"
            py3 framework._synchronous_hook(vim) 
        endfunction

        function! PyVimAutocommandCB(name) 
            echom "called PyVimAutocommandCB"
            py3 framework._autocommand_hook(vim) 
        endfunction
        '''
        self.async_command(func)

    def _handle_synchronous_command(self, vim):
        log('Execing %s' % self._sync_command)
        vim.command(self._sync_command)
        self._sync.set()

    def synchronous_command(self, cmd):
        self._sync_command = cmd
        self._sync = threading.Event()
        self.async_command('call PyVimSyncCB()')
        self._sync.wait()

    def async_command(self, cmd):
        # Use the vim channel
        self.vim_chan.send_command(cmd)

    def _init_autocommand(self, name, context):
        log('init au')
        self.async_command('au {name} * call PyVimAutocommandCB("{name}")'.format(name=name))

    def init_plugins(self):
        for plugin in self.plugins:

            plugin = plugin(self.vim)
            self.plugin_objs.append(plugin)

            for attr_name in dir(plugin):
                attr = getattr(plugin, attr_name)
                # Get the bound function callback
                au_props = AutocomamndProps.get_from(attr)
                if au_props is None:
                    continue

                self.plugin_autocommands.setdefault(plugin, {})
                self.plugin_autocommands[plugin].setdefault(au_props.name, [])
                self.plugin_autocommands[plugin][au_props.name] = au_props

    def init_all_autocommands(self):
        '''
        Initialize autocommand hooks within vim.
        '''
        for au_map in self.plugin_autocommands.values():
            for au_prop in au_map.values():
                self._init_autocommand(au_prop.name, au_prop.context)

    def handle_autocommand(self, au_name):
        for au_map in self.plugin_autocommands.values():
            if au_name in au_map:
                au_map[au_name].func()

@plugin
class ExamplePlugin:
    def __init__(self, vim):
        self.vim = vim
        sync_command('echom "hello"')

    @autocommand("TabNew")
    def handle_focus(self):
        log('Inner handle focus')
